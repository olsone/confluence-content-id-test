Simple Confluence add-on that displays 'content' and 'page' parameters
======================================================================
* Original question: https://answers.atlassian.com/questions/40348084/replacement-for-deprecated-page.-confluence-context-parameters
* Connect documentation: https://developer.atlassian.com/static/connect/docs/latest/concepts/context-parameters.html#additional-parameters-confluence
* Demo install URL: https://confluence-content-id-test.firebaseapp.com/atlassian-connect.json
* Macro key: `content-and-page-parameters`
* Macro name: `Content and Page parameters`