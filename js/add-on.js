'use strict';

var parms = {};
$.each($(window.location.search.substring(1).split('&')), function(i, pair) {
    if (pair) {
        pair = pair.split('=');
        parms[pair[0]] = window.decodeURIComponent(pair[1].replace(/\+/g, '%20'));
    }
});

$.getScript(parms.xdm_e + parms.cp + '/atlassian-connect/all.js', function() {
    var displayed = ['page.id', 'page.version', 'page.type', 'content.id', 'content.version', 'content.type', 'content.plugin'];
    $(displayed).each(function(i, parm) {
        $('#parameter-table tbody').append(AJS.template.load('parameter-rows').fill({parm: parm, value: parms[parm]}));
    });
});